<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        .form__search {
            max-width: 800px;
            margin: 0 auto;
            padding: 40px 60px;
        }
        .form__heading {
            margin-bottom: 40px;
            width: 60%;
            margin: 0 auto;

        }
        .form__item {
            margin-bottom: 20px;
            display: flex;
            align-items: center;
            gap: 20px;
        }
        .form__label {
            min-width: 60px;
            color: #2f2e2f;
            font-weight: 500;
            display: inline-block;
        }
        .form__input {
            width: 100%;
            border: 1.5px solid #40719c;
            outline: none;
            border-radius: 2px;
            padding: 6px;
            background-color:#e1eaf4
        }
        .select {
            width: 100%;
            position: relative;
        }
        .form__select {
            appearance: none;
            cursor: pointer;
        }
        .focus {
            position: absolute;
            display: block;
            top: 75%;
            transform: translateY(-75%);
            right: 10px;
            border-width: 10px;
            border-style: solid;
            border-color: #2f75b6 transparent transparent;
        }
        .form__submit {
            padding: 10px 30px;
            border-radius: 8px;
            background-color: #4e81bd;
            color: #fff;
            border: 3px solid #416c9f;
            margin: 40px auto;
            display: block;
            cursor: pointer;
        }
        .form__add {
            float: right;
            margin:0 20px 20px;
        }
        .form__add-link {
            text-decoration:none;
            cursor: pointer;
            text-align: center;
            padding: 8px 20px;
            border-radius: 8px;
            background-color: #4e81bd;
            color: #fff;
            border: 3px solid #416c9f;
        }
        .form__table {
            width: 100%;
        }
        .form__col {
            text-align: left;
        }
        .form__row .form__col{
            padding-bottom: 20px;
            
        }
        .form__no {
           padding-left: 10px;

        }
        .form__name {
            width: 25%;
        }
        .form__action {
            display: flex;
            gap: 10px;
        }
        thead .form__row .form__col:last-child{
            float: right;
            margin-right: 25%;
        }
        .form__action {   
            float: right;

        }
        .form__button {
            color: #fff;
            border: none;
            background-color:#8cabd0;
            padding: 8px 12px;
            border: 1px solid #416c9f;
            border-radius: 3px;
            cursor: pointer;
        }
    </style>
    <body>
        <form method="post" action="" class="form__search">
            <header class="form__heading">
                <div class="form__item">
                    <label for="select" class="form__label"> Khoa</label>
                    <div class="select">
                        <select id="select" class="form__input form__select">
                            <option value=""></option>
                            <option value="MAT">Khoa học máy tính</option>
                            <option value="KDL">Khoa học vật liệu</option>
                        </select>
                        <span class="focus"></span>
                    </div>
                </div>
                <div class="form__item">
                    <label for="key" class="form__label">Từ khóa</label>
                    <input type="text" id="key" class="form__input">
                </div>
                 <input type="submit" value="Tìm kiếm" class="form__submit">
            </header>
            <div class="form__body">
                <div class="form__amount">
                    Số sinh viên tìm thấy:
                    <span class="form__amount-number">XXX</span>
                </div>
                <div class="form__add">
                    <a href="register.php" class="form__add-link">Thêm</a>
                </div>
                <table class="form__table">
                    <thead>
                        <tr class="form__row">
                            <th class="form__col">No</th>
                            <th class="form__col">Tên sinh viên</th>
                            <th class="form__col">Khoa</th>
                            <th class="form__col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="form__row">
                            <td class="form__col form__no">1</td>
                            <td class="form__col form__name">Nguyễn Văn A</td>
                            <td class="form__col form__class">Khoa học máy tính</td>
                            <td class="form__col form__action">
                                <input type="submit" value="Xóa" class=" form__remove form__button">
                                <input type="submit" value="Sửa" class=" form__edit form__button">
                            </td>
                        </tr>
                         <tr class="form__row">
                            <td class="form__col form__no">2</td>
                            <td class="form__col form__name">Trần Thị B</td>
                            <td class="form__col form__class">Khoa học máy tính</td>
                            <td class="form__col form__action">
                                <input type="submit" value="Xóa" class=" form__remove form__button">
                                <input type="submit" value="Sửa" class=" form__edit form__button">
                            </td>
                        </tr>
                         <tr class="form__row">
                            <td class="form__col form__no">3</td>
                            <td class="form__col form__name">Nguyễn Hoàng C</td>
                            <td class="form__col form__class">Khoa học vật liệu</td>
                            <td class="form__col form__action">
                                <input type="submit" value="Xóa" class=" form__remove form__button">
                                <input type="submit" value="Sửa" class=" form__edit form__button">
                            </td>
                        </tr>
                         <tr class="form__row">
                            <td class="form__col form__no">4</td>
                            <td class="form__col form__name">Đinh Quang D</td>
                            <td class="form__col form__class">Khoa học vật liệu</td>
                            <td class="form__col form__action">
                                <input type="submit" value="Xóa" class=" form__remove form__button">
                                <input type="submit" value="Sửa" class=" form__edit form__button">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </form>
        <script src="" async defer></script>
    </body>
    
</html>
